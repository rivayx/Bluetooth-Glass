﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HistoryModal : MonoBehaviour
{
	[SerializeField]
	private List<Button> _Category;
	[SerializeField]
	private Text _Date;


	[SerializeField]
	private Text _DateResult;
	[SerializeField]
	private Text _Result;
    [SerializeField]
    private Text _Ratio;
    [SerializeField]
    private Text _ResultRatio;

	void Start ()
    {
		_Date.text = System.DateTime.Now.ToString("yyyy.MM.dd");
        _DateResult.text = System.DateTime.Now.ToString("yyyy.MM.dd");

        RegisterModal();
	}

    private void RegisterModal()
	{
		_Category[0].onClick.AddListener(HourlyAction);
		_Category[1].onClick.AddListener(DailyAction);
		_Category[2].onClick.AddListener(WeeklyAction);
		_Category[3].onClick.AddListener(MonthlyAction);
    }

    private void BoldTextButton(Button btn)
    {
        foreach (Button bttn in _Category)
		{
			bttn.transform.GetComponentInChildren<Text>().fontStyle = FontStyle.Normal;
        }
        btn.transform.GetComponentInChildren<Text>().fontStyle = FontStyle.Bold;
    }

    private void HourlyAction()
    {
        BoldTextButton(_Category[0]);
    }
    private void DailyAction()
	{
		BoldTextButton(_Category[1]);
    }
    private void WeeklyAction()
	{
		BoldTextButton(_Category[2]);
    }
    private void MonthlyAction()
	{
		BoldTextButton(_Category[3]);
    }
}
