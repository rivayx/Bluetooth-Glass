﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragIcon : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    [SerializeField]
    private Transform _RightPosition;
    [SerializeField]
    private Transform _LeftPosition;

    bool _Drag;

    public void OnBeginDrag(PointerEventData eventData)
    {
        _Drag = true;
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = eventData.position;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        _Drag = false;
    }

    private void Update()
    {
        if (!_Drag)
        {
            float newPos;
			if (transform.localPosition.x >= 0) // kanan
			{
                newPos = _RightPosition.position.x;
			}
			else
			{
				newPos = _LeftPosition.position.x;
			}

            float newX = Mathf.Lerp(transform.position.x, newPos, Time.deltaTime * 8);
            transform.position = new Vector3(newX, transform.position.y);
        }
    }
}
