﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using BTGlassBase;

using leithidev.unityassets.nativebt.android.entities;
using System;

public class SendMessageBT : MonoBehaviour
{
    [SerializeField]
    private PopupClass _PopupBG;
    [SerializeField]
    private PairedDeviceButton _PairedDeviceListButton;

    [SerializeField]
    private GameObject _ObjectOutput;
    [SerializeField]
    private Button _ConnectBT;
    [SerializeField]
    private Button _ButtonMenu;

    [SerializeField]
    private Image _Background;
    [SerializeField]
    private Button _ToChrome;
    [SerializeField]
    private Sprite _Home;
    [SerializeField]
    private Sprite _Chrome;
    [SerializeField]
    private GameObject _Turtle;
    [SerializeField]
    private Button _Latian;
    [SerializeField]
    private GameObject _LatianObject;
    [SerializeField]
    private Button _ToVideo;

    [SerializeField]
    private Sprite _NormalSprite;
    [SerializeField]
    private Sprite _WarningSprite;
    [SerializeField]
    private Sprite _DangerSprite;

    private float timeResult;

    private LWBluetoothDevice _connectedTo;
    private string uuid = "00001101-0000-1000-8000-00805F9B34FB";
    private string name = "HC-06";

    //private void OnApplicationFocus(bool focus)
    //{
    //    if (!focus)
    //        Handheld.Vibrate();
    //}

    void Start()
    {

#if !UNITY_EDITOR
        NativeBTRuntime.NBTR.BTHandler.BTEventsHandler.BTMessageReceived += OnMessageReceived;
		NativeBTRuntime.NBTR.BTHandler.BTEventsHandler.BTDeviceConnected += OnBtDeviceConnected;
		//NativeBTRuntime.NBTR.BTHandler.BTEventsHandler.BTDeviceDisconnected += OnBtDeviceDisconnected;

#endif

        //try
        //{
        //    AndroidJavaClass jav = new AndroidJavaClass("leithidev.unityassets.nativebt.android.btlib.PluginClass");

        //    _ConnectTo.text = jav.CallStatic<string>("callString", "tes");
        //}
        //catch (Exception E)
        //{
        //    _ConnectTo.text = E.ToString();
        //}
        //_ObjectOutput.gameObject.SetActive(false);

        RegisterModal();
	}

    private void RegisterModal()
    {
        _Latian.onClick.AddListener(LatihanOpen);
        _ToVideo.onClick.AddListener(toVideo);
        _ToChrome.onClick.AddListener(() => _Background.sprite = _Chrome);
        _ConnectBT.onClick.AddListener(OnPairedDevicesClicked);
        _ButtonMenu.onClick.AddListener(_PopupBG.MenuControl);
    }

    private void LatihanOpen()
    {
        _LatianObject.SetActive(true); _PopupBG.CloseAllPopup();
    }

    private void toVideo()
    {
        Handheld.PlayFullScreenMovie("latihan.mp4", Color.black, FullScreenMovieControlMode.Minimal, FullScreenMovieScalingMode.AspectFit);
    }

	void Update () 
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            if (_PopupBG.GetPopup())
            {
                _PopupBG.CloseAllPopup();
            }
            else
            {
                Application.Quit();
                //Debug.Log("Not Focus");
                //Application.runInBackground = true;
            }
        }

        Turtle();
	}

    private void OnMessageReceived(string msg)
    {
        float z = (StaticFunction.GetFloatGyroGlass(msg, GyroAngle.Z) - 31) * 4;

        //_ConnectTo.text = z.ToString();
        if (z > 0 && z < 40)
        {
            _ObjectOutput.GetComponent<Image>().sprite = _NormalSprite;
            timeResult = 0;
		}
		else if (z > 40 && z < 55)
		{
            _ObjectOutput.GetComponent<Image>().sprite = _WarningSprite;
            timeResult = 0;
		}
		else if (z > 55 && z < 70)
		{
            _ObjectOutput.GetComponent<Image>().sprite = _DangerSprite;
            if (timeResult == 0)
                timeResult = Time.time + 10;
		}
        else
		{
            _ObjectOutput.GetComponent<Image>().sprite = _NormalSprite;
            timeResult = 0;
        }
	}

    private void Turtle()
    {
        if (timeResult < Time.time && timeResult != 0)
        {
            _Turtle.SetActive(true);
        }

    }

	private void OnBtDeviceConnected(LWBluetoothDevice device)
	{
        _connectedTo = device;
        _Background.gameObject.SetActive(true);

        _ObjectOutput.gameObject.SetActive(true);
	}

	private void OnPairedDeviceButtonClicked(LWBluetoothDevice btDevice)
	{
		NativeBTRuntime.NBTR.BTWrapper.Connect(btDevice, uuid);
        //_PopupBG.ListBTControl();
	}

	private void OnPairedDevicesClicked()
	{
		if (!NativeBTRuntime.NBTR.BTWrapper.GetBTAdapter().IsEnabled())
		{
			NativeBTRuntime.NBTR.BTWrapper.ShowBTEnableRequest();
		}
		else
		{
            foreach (LWBluetoothDevice device in NativeBTRuntime.NBTR.BTWrapper.GetPairedDevices())
			{
                if (device.GetName() == name)
                {
                    //konek
                    OnPairedDeviceButtonClicked(device);
                    break;
                }
            }
		}
	}
}
