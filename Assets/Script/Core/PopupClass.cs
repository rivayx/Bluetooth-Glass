﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupClass : MonoBehaviour
{
    private Animator _Animate;
    private float _Time;

    private void Awake()
    {
        _Animate = gameObject.GetComponent<Animator>();
        transform.SetAsFirstSibling();
    }

    public void ListBTControl()
    {
        if (_Animate.GetBool("ListBT") == false)
        {
            transform.SetAsLastSibling();
            _Animate.SetBool("ListBT", true);
        }
        else
        {
			_Animate.SetBool("ListBT", false);
			_Time = Time.time + 1;
        }
    }

    public void MenuControl()
	{
        if (_Animate.GetBool("Menu") == false)
		{
			transform.SetAsLastSibling();
            _Animate.SetBool("Menu", true);
        }
        else
        {
			_Animate.SetBool("Menu", false);
			_Time = Time.time + 1;
        }
    }

    private void Update()
    {
        if (_Animate.GetBool("Menu") == false && _Animate.GetBool("ListBT") == false)
        {
            if (_Time < Time.time)
			{
				transform.SetAsFirstSibling();
            }
        }
    }

    public void CloseAllPopup()
    {
        _Animate.SetBool("Menu", false);
		_Animate.SetBool("ListBT", false);
		_Time = Time.time + 1;
    }

    public bool GetPopup()
    {
        return _Animate.GetBool("Menu") || 
               _Animate.GetBool("ListBT");
    }
}
