﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BTGlassBase
{
    public struct CONST_VAR
    {
        
    }

    public enum GyroAngle
    {
        Null = 0,
        X = 1,
        Y = 2,
        Z = 3
    }
}
