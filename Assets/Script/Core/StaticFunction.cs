﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BTGlassBase;

public class StaticFunction
{
    public static float GetFloatGyroGlass(string val, GyroAngle angel)
    {
        float result = 0f;
        string[] stringArray = new string[val.Length];

		for (int i = 0; i < val.Length; i++)
		{
			stringArray[i] = val.Substring(i, 1);
		}
        int first;

        switch(angel)
		{
			case GyroAngle.X:
                first = 3;
				break;
			case GyroAngle.Y:
				first = 14;
				break;
			case GyroAngle.Z:
                first = 25;
				break;
            default:
                first = 0;
                break;
        }

        int last = first + 5;
        string resultString = "";

        for (int i = first; i < last; i++)
        {
            resultString += stringArray[i];
        }

        result = float.Parse(resultString);
        return result;
    }
}