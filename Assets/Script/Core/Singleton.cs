﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton : MonoBehaviour 
{
	public static Singleton Instance { get; private set; }

	void Awake()
	{
		if (Instance == null)
		{
			Instance = this;

			if (Instance == null)
			{
				Instantiate(Instance);
				Debug.Log("error when trying to create singleton");
			}
			DontDestroyOnLoad(gameObject);
		}
		else
		{
			Destroy(gameObject);
		}

	}
}
